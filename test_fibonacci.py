#!/usr/bin/env python3

import pytest
from fibonacci import fibonacci_recursive, fibonacci_loop, fibonacci_moivre_binet

@pytest.mark.parametrize("fib", [fibonacci_recursive, fibonacci_loop, fibonacci_moivre_binet])
@pytest.mark.parametrize("n,f_n", [(0, 0),
                                   (1, 1),
                                   (2, 1),
                                   (100, 354224848179261915075),
                                   ])
def test_n(fib, n, f_n):
    if fib is fibonacci_moivre_binet and n > 50:
        pytest.skip("fibonacci_moivre_binet does not work for n > 50 due to floating point arthmetic")
    assert fib(n) == f_n
