#!/usr/bin/env python3

from functools import cache


@cache
def fibonacci_recursive(n):
    if n < 2:
        return n
    return fibonacci_recursive(n - 2) + fibonacci_recursive(n - 1)


def fibonacci_loop(n):
    if n < 1:
        return 0
    a, b = 0, 1
    for i in range(n-1):
        a, b = b, a + b
    return b


def fibonacci_moivre_binet(n):
    sqrt5 = 5**0.5
    return round((((1 + sqrt5)/2)**n)/sqrt5)


if __name__ == "__main__":
    for fib in [fibonacci_recursive, fibonacci_loop, fibonacci_moivre_binet]:
        print(f"{fib.__name__:25} {[fib(n) for n in range(100, 103)]}")
